<?php

namespace app\models;

use yii\db\ActiveRecord;

class Book extends ActiveRecord
{

  public function rules()
  {
      return [
          [['book_name', 'author_id'], 'required'],
      ];
  }

  public function attributeLabels()
  {
      return [
          'book_name' => 'Название книги', 'author_id' => 'Имя автора',
      ];
  }
}
