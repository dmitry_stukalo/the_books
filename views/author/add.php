<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
?>
<h1>Создание автора</h1>
<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name') ?>

    <div class="form-group">
        <?= Html::submitButton('Создать автора', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
