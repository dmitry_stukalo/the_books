<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Список авторов</h1>
<ul>

<?php foreach ($authors as $author): ?>

    <li>
        <?= Html::encode("{$author->name}") ?>:
        <?php $nn = 1?>
        <?php foreach ($books as $book): ?>
          <?php if ($author['author_id'] == $book['author_id']): ?>
          <?php $pp = $nn++?>
        <?php else: $pp = $nn - 1   ?>
          <?php endif; ?>
        <?php endforeach; ?>
        <?=$pp?>
    </li>


<?php endforeach; ?>
</ul>
