<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Список книг</h1>
<ul>
<?php foreach ($books as $book): ?>
    <li>
        <?= Html::encode("{$book->book_name}") ?>:
    <?php foreach ($authors as $author): ?>
      <?php if ($author->author_id == $book ->author_id): ?>
          <?= Html::encode("{$author->name}")?>
      <?php endif; ?>
    <?php endforeach; ?>
    </li>
<?php endforeach; ?>
</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>
