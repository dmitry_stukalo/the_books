<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
?>
<h1>Добавление книги</h1>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'book_name') ?>
    <?= $form->field($model, 'author_id')->dropDownList($items); ?>

    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
