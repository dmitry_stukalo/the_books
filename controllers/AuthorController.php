<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\Authors;
use app\models\Book;

class AuthorController extends Controller
{
    public function actionAdd()
    {

    $model= new Authors();

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {

      $model->save();

      return $this-> refresh();

     } else {

      return $this->render('add', ['model' => $model]);

      }
    }

    public function actionAll()
    {

    $model= new Authors();
    $books = Book::find()-> all();
    $authors = Authors::find() -> all();
    $counts = Book::find($model->author_id)->asArray()->all();




    return $this->render('all', [
    'books' => $books, 'authors' => $authors, 'model' => $model, 'counts' => $counts
    ]);

    }
}
