<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use	yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use app\models\Book;
use app\models\Authors;


class BooksController extends Controller
{


    public function actionIndex()
    {

    $model= new Book();
    $authors = Authors::find()->all();
    $items = ArrayHelper::map($authors,'author_id','name');

    if ($model->load(Yii::$app->request->post()) && $model->validate()) {

      $model->save();
      return $this-> refresh();
      // return $this->render('add', ['model' => $model] );
     } else {

      return $this->render('add', ['model' => $model,'items' => $items]);

      }
    }

      public function actionAll()
      {

      $authors = Authors::find()->all();
      $query = Book::find();

      $pagination = new Pagination([
          'defaultPageSize' => 10,
          'totalCount' => $query->count(),
      ]);

      $books = $query->orderBy('author_id')
          ->offset($pagination->offset)
          ->limit($pagination->limit)
          ->all();

        return $this->render('all', [
            'books' => $books,
            'pagination' => $pagination,
            'authors' => $authors
        ]);
      }
}

class BookController extends ActiveController
{
    public $modelClass = 'app\models\Book';
}
